import UIKit
class ProductCell: UITableViewCell {
    @IBOutlet var title: UILabel!
    @IBOutlet var id: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var thumbnail: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
